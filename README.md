## Site d'affichage des données du projet NAMA

### Description site
Ce site a pour but de présenter les données du projet NAMA sous une forme visuellement accessible, et de proposer diverses options de tri et de filtre sur ces données.

### Outils utilisés
- [Vue.js](https://vuejs.org/) v2
- [Chart.js](https://www.chartjs.org/)
- [vue-chartjs](https://vue-chartjs.org/fr-fr/)
- [Bootstrap](https://getbootstrap.com/)

### Installation
Les outils utilisés doivent être chargés depuis leur origine web; le site doit donc être déposé sur un serveur disposant d'une connexion Internet.

### Instructions
![Page principale](assets/img/img1.png "Page principale")
*Page principale du site*

Cette page présente les données sous la forme d'un tableau à deux colonnes: la forme de référence dans la colonne de gauche, et les formes dérivées dans la colonne de droite. Les formes dérivées sont colorées en fonction de leur type : bleu pour les variations, vert pour les synonymes, et rouge pour les traductions. La langue de chacune des formes est affichée dans un cadre blanc suivant la forme. Les quatre boutons en haut de la page permettent d'utiliser différents types de filtres et de traitement sur ces données, ainsi que d'afficher des données additionnelles si nécessaire.

![Filtration des réferences](assets/img/img2.png "Filtration des réferences")
*Filtration des réferences*

Le premier bouton affiche des options permettant de trier les références. Deux types de tri sont disponibles: le premier type permet de trier les références en fonction de leur langue, et le second permet de les trier manuellement par une recherche textuelle qui cachera toute référence ne correspondant pas au terme de recherche.
Ces deux options peuvent être combinées pour si nécessaire pour affiner la recherche encore plus.

![Filtration des formes](assets/img/img3.png "Filtration des formes")
*Filtration des formes*

Le deuxième bouton affiche des options permettant de trier les formes. Ici, trois types de tri sont disponibles: le premier type permet de trier les formes en fonction de leur langue, le deuxième permet de les trier en fonction de leur type, et le troisième permet de les trier en fonction de leur source bibliographique. Ces filtres peuvent tous trois être combinés. Pour activer les filtres sélectionnés, il faut cliquer sur le bouton vert portant la mention "Appliquer les paramètres de filtre sélectionnés"

![Options](assets/img/img4.png "Options")
*Options*

Le troisième bouton affiche des options spéciales qui permettent d'afficher des informations additionnelles. La première option permet d'afficher des graphes représentant la distribution des types de formes dérivées (voir image ci-dessous). La deuxième option permet de cacher toutes références n'ayant aucune forme correspondante, soit car la référence n'a aucune forme correspondante selon les donnée, soit car toutes les formes ont été cachées par des options de filtration.
La troisième option permet d'ajouter des colonnes au tableau indiquant le nombre de chaque type de forme pour chaque référence (voir deuxième image ci-dessous). Ces colonnes peuvent être triées par ordre croissant ou décroissant. Le dernier bouton permet d'afficher les sources de chaque forme sous la forme d'un cadre suivant chaque forme (voir troisième image ci-dessous).

![Graphes](assets/img/img5.png "Graphes")
*Graphes*

![Colonnes](assets/img/img6.png "Colonnes")
*Colonnes*

![Sources](assets/img/img7.png "Sources")
*Sources*

Le dernier bouton, le bouton "Reset" désactive toutes les options, affichant de nouveau la version du site sans aucun filtre, tri ou informations supplémentaires présentée au chargement de la page.
