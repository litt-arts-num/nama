import Tableline from "./components/tableline.js"
import Piegraph from "./components/piegraph.js"

var tableline = Vue.component = ("tableline", Tableline)
var piegraph = Vue.component = ("piegraph", Piegraph)
var appli = new Vue({
  el: "#vue",
  data: {
    datalist: [],
    basedatalist: [],
    searchtext: "",
    language: "",
    listelangues: [],
    checkedlangues: [],
    checkedlanguesform: [],
    listesources: [],
    checkedsources: [],
    listeformes: [],
    checkedformes: [],
    showsort: false,
    showrefsort: false,
    showformsort: false,
    showformtypesort: false,
    showsource: true,
    hidezero: true,
    showGraph: true,
    showGroups: true,
    showTypes: true,
    showSpecies: true,
    formText: "",
    groupText: "",
    typeText: "",
    especeText: "",
  },
  components: {
    tableline: tableline,
    piegraph: piegraph
  },
  methods: {
    loadjson() {
      var that = this
      const xhttp = new XMLHttpRequest();
      xhttp.onload = function() {
        var parsedjson = JSON.parse(this.responseText).tei_part.entry
        that.datalist = parsedjson
        that.basedatalist = parsedjson
        that.datalist.forEach((item) => {
          var reflanguage = ""
          var source = ""
          var reference = []
          var translations = []
          var variations = []
          var synonyms = []

          item.groups = []
          item.types = []
          item.especes = []

          if (item.hasOwnProperty('def')) {
            item.def.forEach((def) => {
              if (def.hasOwnProperty('trait')) {
                def.trait.forEach((t) => {

                  if (t.label == "groupe") {
                    item.groups.push({"text" : t.desc["__text"], "src": def._source})
                  }

                  if (t.label == "type") {
                    let type
                    if (type = item.types.find(type => type.text == t.desc)) {
                        type.src.push(def._source)
                    } else {
                        item.types.push({"text" : t.desc, "src": [def._source]})
                    }
                  }

                  if (t.label == "espèce") {
                    item.especes.push({"text" : t.desc["__text"], "src": def._source})
                  }


                });
              }
            });
          }

          item.form.forEach((forme) => {
            if (forme['_type'] == 'reference') {
              reflanguage = forme["_xml:lang"]
              reference = forme
            } else if (forme['_xml:lang'] == reflanguage) {
              variations.push(forme.orth)
              source = forme["_source"]
            } else if (forme['_xml:lang'] != reflanguage) {
              translations.push(forme.orth)
              source = forme["_source"]
            }
            if (typeof forme.lbl != 'undefined') {
              synonyms.push(forme.lbl["__text"])
              source = forme["_source"]
            }
          })
          item.reference = reference
          if (!that.listelangues.includes(reflanguage)) {
            that.listelangues.push(reflanguage)
            that.checkedlangues.push(reflanguage)
            that.checkedlanguesform.push(reflanguage)
          }
          var sourcelist = source.split(",")
          sourcelist.forEach((item, i) => {
            if (item != "") {
              item = item.trim()
              if (item[0] != "#") {
                item = "#bib" + item
              }
              sourcelist[i] = item
            }
          })
          sourcelist.forEach((item) => {
            if (!that.listesources.includes(item) && item != "") {
              that.listesources.push(item)
              that.checkedsources.push(item)
            }
          })
          that.listesources.sort((a, b) => a.localeCompare(b, "en", {
            numeric: true
          }))
          that.checkedsources.sort((a, b) => a.localeCompare(b, "en", {
            numeric: true
          }))

          that.listeformes = ["traductions", "variations (synonymes inclus)", "synonymes"]
          that.checkedformes = ["traductions", "variations (synonymes inclus)", "synonymes"]
          item.variations = variations.length
          item.traductions = translations.length
          item.synonyms = synonyms.length
          item.total = item.variations + item.traductions + item.synonyms

        });
      }
      xhttp.open("GET", "NAMA_tei.json", true)
      xhttp.send()
    },

    sortTotalAsc() {
      this.datalist.sort((a, b) => a.total - b.total)
    },

    sortTotalDesc() {
      this.datalist.sort((a, b) => b.total - a.total)
    },

    sortVarAsc() {
      this.datalist.sort((a, b) => a.variations - b.variations)
    },

    sortVarDesc() {
      this.datalist.sort((a, b) => b.variations - a.variations)
    },

    sortSynAsc() {
      this.datalist.sort((a, b) => a.synonyms - b.synonyms)
    },

    sortSynDesc() {
      this.datalist.sort((a, b) => b.synonyms - a.synonyms)
    },

    sortTradAsc() {
      this.datalist.sort((a, b) => a.traductions - b.traductions)
    },

    sortTradDesc() {
      this.datalist.sort((a, b) => b.traductions - a.traductions)
    },

    sortIDAsc() {
      this.datalist.sort((a, b) => a["_xml:id"].localeCompare(b["_xml:id"], "en", {
        numeric: true
      }))
    },

    sortIDDesc() {
      this.datalist.sort((a, b) => b["_xml:id"].localeCompare(a["_xml:id"], "en", {
        numeric: true
      }))
    },

    sortForms() {
      this.datalist = JSON.parse(JSON.stringify(this.basedatalist))
      this.datalist.forEach((item) => {
        var reflanguage = ""
        var reference = []
        var translations = []
        var variations = []
        var synonyms = []
        var pos = 0
        var todel = []
        item.form.forEach((forme) => {
          if (forme['_type'] != 'reference') {
            var sourcelist = forme['_source'].split(",")
            sourcelist.forEach((item, i) => {
              if (item != "") {
                item = item.trim()
                if (item[0] != "#") {
                  item = "#bib" + item
                }
                sourcelist[i] = item
              }
            })
          }
          if (forme['_type'] == 'reference') {
            reflanguage = forme["_xml:lang"]
            reference = forme
          } else if (forme['_xml:lang'] == reflanguage && this.checkedlanguesform.includes(forme['_xml:lang']) && sourcelist.every(element => this.checkedsources.includes(element))) {
            if (this.checkedformes.includes("variations (synonymes inclus)")) {
              variations.push(forme.orth)
            } else {
              todel.push(pos)
            }
          } else if (forme['_xml:lang'] != reflanguage && this.checkedlanguesform.includes(forme['_xml:lang']) && sourcelist.every(element => this.checkedsources.includes(element))) {
            if (this.checkedformes.includes("traductions")) {
              translations.push(forme.orth)
            } else {
              todel.push(pos)
            }
          }
          if (typeof forme.lbl != 'undefined' && this.checkedlanguesform.includes(forme['_xml:lang']) && sourcelist.every(element => this.checkedsources.includes(element))) {
            if (this.checkedformes.includes("synonymes")) {
              synonyms.push(forme.lbl["__text"])
            } else {
              forme.lbl = undefined
            }
            if (!this.checkedformes.includes("variations (synonymes inclus)")) {
              forme.lbl = undefined
            }


          }
          if (forme['_type'] != 'reference') {
            if (!this.checkedlanguesform.includes(forme['_xml:lang']) || !sourcelist.every(element => this.checkedsources.includes(element))) {
              if (!todel.includes(pos)) {
                todel.push(pos)
              }
            }
          }
          pos++
        });
        todel.sort((a, b) => b - a)
        todel.forEach((num) => {
          item.form.splice(num, 1)
        });
        if (!this.checkedformes.includes("variations (synonymes inclus)")) {
          synonyms = []
        }
        item.reference = reference
        item.variations = variations.length
        item.traductions = translations.length
        item.synonyms = synonyms.length
        item.total = item.variations + item.traductions + item.synonyms
      })
    },

    resetSort() {
      this.datalist.sort((a, b) => a["_xml:id"].localeCompare(b["_xml:id"], "en", {
        numeric: true
      }))
      this.checkedlangues = this.listelangues
      this.checkedlanguesform = this.listelangues
      this.checkedsources = this.listesources
      this.checkedformes = this.listeformes
      this.hidezero = true
      this.showsort = false
      this.showsource = true
      this.showGraph = true
      this.searchtext = ""
      this.sortForms()
    },

    displayItem(item) {
      if (this.checkReference(item) && this.checkForm(item) && this.checkGroup(item) && this.checkType(item)
      && this.checkEspece(item) && (!this.hidezero || item.total > 0)) {
        return true
      }

      return false
    },

    checkReference(item){
      if ( (this.searchtext == '' || item.reference.orth.includes(this.searchtext)) && this.checkedlangues.includes(item.reference['_xml:lang']) ) {
        return true
      }
      return false
    },

    checkForm(item) {
      if (this.formText != "") {
        let found = false
        item.form.forEach((forme) => {
          if (forme["_type"] != 'reference') {
            if (forme["orth"].includes(this.formText)) {
              found = true
              return
            }
          }
        })
        return found
      }

      return true
    },

    checkEspece(item) {
      if (this.especeText == "" || item.especes.find(espece => espece.text.includes(this.especeText))) {
        return true
      }

      return false
    },

    checkGroup(item) {
      if (this.groupText == "" || item.groups.find(group => group.text.includes(this.groupText))) {
        return true
      }

      return false
    },

    checkType(item) {
      if (this.typeText == "" || item.types.find(type => type.text.includes(this.typeText))) {
        return true
      }

      return false
    }
  },
  beforeMount() {
    this.loadjson()
  },
})
/*    appli.component("tableline", Tableline) appli.component("piegraph", Piegraph)
    appli.mount('#vue')*/
