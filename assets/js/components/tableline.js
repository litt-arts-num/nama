import Piegraph from "./piegraph.js"
var piegraph = Vue.component = ("piegraph", Piegraph)
export default {

  props: {
    item: Object,
    datasortlist: Array,
    checkedlanguesform: Array,
    showsort: Boolean,
    showgraph: Boolean,
    showsource: Boolean,
    variationsnum: Number,
    traductionsnum: Number,
    synonymsnum: Number,
    totalvariantes: Number
  },
  data: function() {
    return {
      reflanguage: "b",
      reference: [],
      variations: [],
      translations: [],
      synonyms: [],
      id: ""
    }
  },
  components: {
    piegraph: piegraph
  },
  methods: {
    updateType(txt) {
      this.$emit('updatetype', txt)
    },
    getdata() {
      var source = ""
      this.item.form.forEach((forme) => {
        if (forme['_type'] == 'reference') {
          this.reflanguage = forme["_xml:lang"]
          this.reference = forme
        } else if (forme['_xml:lang'] == this.reflanguage) {
          this.variations.push(forme.orth)
          source = forme["_source"]
        } else if (forme['_xml:lang'] != this.reflanguage) {
          this.translations.push(forme.orth)
          source = forme["_source"]
        }
        if (typeof forme.lbl != 'undefined') {
          this.synonyms.push(forme.lbl["__text"])
          source = forme["_source"]
        }
        if (forme['_type'] != 'reference') {
          var sources = source.split(",")
          sources.forEach((item, i) => {
            if (item != "") {
              item = item.trim()
              if (item[0] != "#") {
                item = "#bib" + item
              }
              sources[i] = item
            }
          })
          forme["modsources"] = sources
        }
      });
      this.id = this.item["_xml:id"]
    }
  },
  beforeMount() {
    this.getdata()
  },
  template: `
  <tr>
  <!--<th scope="row">{{item["_xml:id"]}}</th>-->
  <template v-for="form in item.form">
    <td class="ref" v-if="form['_type']=='reference'">
      <span class="ref">{{form.orth}}</span>
      <br/>
      <span class="badge bg-light text-dark">{{form["_xml:lang"]}}</span>
    </td>
  </template>
  <td>
    <div class="item-forms" v-for="form in item.form">
      <template v-if="form['_type'] != 'reference'">
        <div v-if="form['_xml:lang'] == reflanguage" class="variation item-form">
          {{form.orth}}
          <span class="badge bg-light text-dark">{{form["_xml:lang"]}}</span>
        </div>
        <div v-if="form['_xml:lang'] != reflanguage" class="item-form translation">
          {{form.orth}}
          <span class="badge bg-light text-dark">{{form["_xml:lang"]}}</span>
        </div>
      </template>
      <div v-if="typeof form.lbl != 'undefined'" class="synonym item-form">
        {{form.lbl["__text"]}}
      </div>
      <template v-if="showsource">
        <template v-for="source in form['modsources']">
          <span class="text-muted">{{source}}</span>
        </template>
      </template>
    </div>
  </td>
  <td v-if="showsort">
    {{this.totalvariantes}}
  </td>
  <td v-if="showsort">
    {{this.variationsnum}}
  </td>
  <td v-if="showsort">
    {{this.traductionsnum}}
  </td>
  <td v-if="showsort">
    {{this.synonymsnum}}
  </td>

  <td>
  <template v-for="group in item.groups">
    <div class="subcell">
      {{group.text}} <br/>
      <span v-if="showsource" class="text-muted">{{group.src}}</span>
    </div>
  </template>
  </td>

  <td>
  <template v-for="type in item.types">
    <div class="subcell">
      <span @click="updateType(type.text)">{{type.text}}</span> <br/>
      <span v-if="showsource" v-for="src in type.src" class="ms-1 text-muted">{{src}}</span>
    </div>
  </template>
  </td>

  <td>
  <template v-for="espece in item.especes">
    <div class="subcell">
      {{espece.text}} <br/>
      <span v-if="showsource" class="text-muted">{{espece.src}}</span>
    </div>
  </template>
  </td>

  <td >
  <template v-if="showgraph">
    <div>
      <piegraph v-if="totalvariantes>0" @change="console.log('changed')" :styles="{height:'115px'}" :data={Rsize:traductionsnum,Gsize:synonymsnum,Bsize:variationsnum}></piegraph>
    <div v-else>
      N/A
      </div>
    </div>
  </template>
  </td>
  </tr>
  `
}
