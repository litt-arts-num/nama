export default {
  extends: VueChartJs.Pie,
  props: {
    data: Object
  },
  mounted() {

    this.renderChart(this.datalist, this.options)
  },
  computed: {
    datalist: function() {
      return {
        labels: ['Traductions', 'Synonymes', 'Variations'],
        datasets: [{
          label: 'Dataset 1',
          data: [this.data.Rsize, this.data.Gsize, this.data.Bsize],
          backgroundColor: ['rgba(255, 0, 0, 0.4)',
            'rgba(0, 255, 0, 0.4)',
            'rgba(0, 0, 255, 0.4)',
          ],
        }]
      }
    },
    options: function() {
      return {
        responsive: true,
        maintainAspectRatio: false
      }
    }
  },
  watch: {
    data: function() {
      this.renderChart(this.datalist, this.options)
    }
  }
}